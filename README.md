# SPD Workshop: React - RxJS

Frontend workshop starter. 9/11/19

## 1. Getting Started
###1.1. Installing requirements

* Node 12.13.0 +
* Npm 6.13.0 +
* React 16.11.0 +
* RxJS 6.5.3 +

### 1.2. Project structure

    |- node_modules         : Project dependencies
    |- public               : Public resources
    |- src                  : Project source code folder
        |- app              : Application folder
            |- components   : Shared UI/UX components
            |- containers   : Pages components
            |- enums        : Enums folder
            |- helpers      : Helpers modules
            |- locales      : Languages files
            |- rest         : Services for API calls

## 2. Project installation

For installing packages, you need run command:

    npm install
    
For working with API service, just edit environment api path in the root folder, like:

```
NODE_ENV=local

### React App configuration
REACT_APP_API_PREFIX=/api/v1
REACT_APP_STORAGE_NAME=access-token
REACT_APP_LOADER_COLOR=#2aabe1
```

## 3. Run application and tests

Start development:

    npm start
    
Build application for production:

    npm run build
    
Run tests for application:

    npm run test
    
