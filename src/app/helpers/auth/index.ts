import { get, remove, set } from 'local-storage';

const name = process.env.REACT_APP_STORAGE_NAME as string;

export const getToken = (): string => {
  return get<string>(name) || '';
};

export const saveToken = (token: string): boolean => {
  return set<string>(name, token);
};

export const resetToken = (): void => {
  return remove(name);
};

export const guestGuard = (): boolean => {
  return !getToken();
};

export const portalGuard = (): boolean => {
  return !!getToken();
};
