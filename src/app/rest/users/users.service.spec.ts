import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

import http from './../../helpers/rest/rest.service';

import { getUsersList } from './users.service';

import { UsersParams } from './users.params';
import { UsersListDto } from './users.list.dto';

import { cold } from 'jest-marbles';

describe('UsersService', () => {
  const ajaxGetSpyOn = jest.spyOn(http, 'get');

  beforeEach(() => {
    ajaxGetSpyOn.mockClear();
  });

  test(('getUsersList() should return response'), () => {
    const marbleValues = {
      a: new UsersListDto()
    };

    const receivedMarbles = cold('--a-b|', marbleValues);
    const expeccteddMarbles = cold('--(a|)', marbleValues);

    ajaxGetSpyOn.mockReturnValue(receivedMarbles);

    expect(getUsersList({} as UsersParams)).toBeObservable(expeccteddMarbles);
  });
});

