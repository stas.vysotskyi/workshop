import { Observable } from 'rxjs/internal/Observable';

import Http from 'helpers/rest/rest.service';

import { TokensRequestDto } from './tokens.request.dto';
import { TokensResponseDto } from './tokens.response.dto';

export function createToken(data: TokensRequestDto): Observable<TokensResponseDto> {
  return Http.post<TokensResponseDto>('/tokens', data)
    .pipe(response => Http.responseHandle(response, {
      200: 'Ура, мы в системе',
      400: 'Увы, что-то пошло не так..',
      401: 'К сожалению, у Вас не прав'
    }));
}

export function deleteToken(): Observable<void> {
  return Http.delete<void>('/tokens')
    .pipe(response => Http.responseHandle(response, {
      200: 'Мы вышли с системы',
      400: 'Увы, что-то пошло не так..'
    }));
}
