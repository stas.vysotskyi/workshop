import React, { Fragment } from 'react';
import { Redirect } from 'react-router';
import { Route as ReactRoute } from 'react-router-dom';

import { Props } from './route.types';

const Route: React.FC<Props> = ({ canActive = () => true, redirectTo = '/', ...props  }: Props) => {
  return (
    <Fragment>
      {canActive() ? (
        <ReactRoute {...props} />
      ) : (
        <Redirect from={props.path} to={redirectTo} exact />
      )}
    </Fragment>
  )
};

export default Route;
