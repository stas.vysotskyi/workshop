export interface Props {
  visible: boolean;
  cancel: () => void;
  name: string;
  message: string;
  buttonTitle: string;
  success: () => void;
}
