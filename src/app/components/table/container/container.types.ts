import { Params } from 'helpers/rest/rest.params';

import { Props as ColumnProps } from './../column/column.types';

export interface Props {
  items: any[];
  params?: Params
  update: () => void;
  children: {
    props: ColumnProps
  }[];
}
