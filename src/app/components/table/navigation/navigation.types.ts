import { Params } from 'helpers/rest/rest.params';

export interface Props {
  params: Params
  update: () => void;
  totalPages: number;
  totalItems: number;
}
