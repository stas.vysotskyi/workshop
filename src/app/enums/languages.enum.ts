export enum LanguagesEnum {
  ENGLISH = 'en-GB',
  UKRAINIAN = 'uk-UA'
}
