import React from 'react';

import { saveToken } from 'helpers/auth';

import { createToken } from 'rest/tokens/tokens.service';
import { TokensRequestDto } from 'rest/tokens/tokens.request.dto';

import FormLayout from './form'

import { Props } from './auth.types';

import Styles from './auth.module.scss';

const AuthContainer: React.FC<Props> = ({ history }: Props) => {

  const handleLogin = (email: string, password: string)=> {
    const dto = new TokensRequestDto({ email, password });
    createToken(dto)
      .subscribe(tokens => {
        saveToken(tokens.accessToken);
        history.push('/users');
      });
  };

  return (
    <div className={Styles.container}>
      <div className={Styles.form}>
        <FormLayout loginUser={handleLogin}/>
      </div>
    </div>
  );
};

export default AuthContainer;
