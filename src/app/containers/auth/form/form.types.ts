export interface Props {
  loginUser: (email: string, password: string) => void;
}
